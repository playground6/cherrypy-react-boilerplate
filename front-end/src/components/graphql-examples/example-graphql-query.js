//this is an example of graphql query
import React, { Component } from 'react';
import ApolloClient from "apollo-boost";
import { Query, ApolloProvider } from "react-apollo";
import gql from "graphql-tag";

const client = new ApolloClient({
    uri: "http://localhost:4000"
});

const GET_EMPLOYER = gql`
 query employers{
   employers {
    id
    name
    email
    createdAt
    updatedAt
   }
  }
`;

class GraphqlQuery extends Component {
    render() {
        return (
            <ApolloProvider client={client}>
                <div className="App">
                    <Query query={GET_EMPLOYER}>
                        {/* this is called as a render prop function */}
                        {({ loading, data }) => {
                            if (loading) return 'Loading...'
                            const { employers } = data;
                            return employers.map(employer => <h1>{employer.name}</h1>)
                        }}
                    </Query>
                </div>
            </ApolloProvider>
        );
    }
}

export default GraphqlQuery;
