/* In this example we collect the values entered in the form
and update the values in the database. These are the steps that we follow

1. Create a Form
2. Retrieve the values entered in the form in a function called 
   handleInput(). This function is called during onChange() function. 
3. The values are retrieved in the function and added to the state 
   global variable
4. In the mutation query the state values are passed instead of 
   hardcoding the name and email values

*/

import React, { Component } from 'react';
import ApolloClient from "apollo-boost";
import { Mutation, ApolloProvider } from 'react-apollo';
import gql from "graphql-tag";

const client = new ApolloClient({
    uri: "http://localhost:4000"
});


const ADD_EMPLOYER = gql`
    mutation createEmployer($name: String!, $email: String!){
        createEmployer(data: {
            name: $name,
            email: $email
        }) {
            id
            name
            email
            createdAt
            updatedAt
            }
    }
`

class MutationExampleWithForm extends Component {

    state = {
        name: '',
        email: '',
    }

    handleInput = (e) => {
        //this is how to create an empty object in javascript
        const formData = {};
        //e.target.value is the entered value by the user
        //e.target = <input name = "name" type = "text" value = { name } onChange = { this.handleInput } />
        //Now if you want to take the value from the above we use e.target.value
        /* e.target.name is the name of the input box eg        
        formData[name] = jeril or formData[email] = jerilcj3@gmail.com */
        formData[e.target.name] = e.target.value;
        // assigning formData array to state.
        /* The below is called a spread operator. This copies the array formData 
           into another array called state */
        /* state should always be updated using the setState function */
        this.setState({ ...formData })
    };

    render() {
        const { name, email } = this.state

        return (
            <ApolloProvider client={client}>
                <div className="App">
                    { /* Wrap form within the mutation component */}
                    <Mutation
                        // the below are props which we pass to the mutation component
                        mutation={ADD_EMPLOYER}
                        variables={{
                            name,
                            email
                        }}
                    >
                        { /* We would be passing the <form> as a render prop to the mutation component */}
                        { /* render prop can be defined like this {() => ( PUT FORM CODE HERE)} */}
                        { /* The value that is coming out of the render prop is a function or in other words 
                            render prop returns a function. This function is nothing but the query itself.
                            When this function is called it calls the graphql query itself. The name of the 
                            function that is returned and the graphql query will always be the same. Here the
                            name of the returned function is createEmployer() */ }
                        {createEmployer => (
                            <form onSubmit={(e) => {
                                {/* this would prevent the form from refreshing the page. This can be helpful
                                    to display the errors occurred during form submit  */ }
                                e.preventDefault();
                                {/* the below code is a promise. If createEmployer() is successful then 
                                    the form gets reloaded. If the form is reladed then the state should be 
                                    set empty for the next set of values to be entered and captured.
                                */ }
                                createEmployer().then(() => {
                                    this.setState({
                                        name: '',
                                        email: ''
                                    })
                                }).catch(e => console.log(e))
                            }}>
                                <input
                                    name="name"
                                    type="text"
                                    value={name}
                                    onChange={this.handleInput}
                                    placeholder="name" />

                                <input
                                    name="email"
                                    type="text"
                                    value={email}
                                    onChange={this.handleInput}
                                    placeholder="email" />

                                <button type="submit">Add Employer</button>
                            </form>
                        )}
                    </Mutation>
                </div>
            </ApolloProvider>
        );
    }
}

export default MutationExampleWithForm;


